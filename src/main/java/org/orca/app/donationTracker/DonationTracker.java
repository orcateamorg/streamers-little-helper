package org.orca.app.donationTracker;

import org.orca.app.donationTracker.engine.TrackEngine;
import org.orca.app.donationTracker.enums.choices.UserFileChoices;
import org.orca.app.donationTracker.modals.User;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class DonationTracker {


    private static final String clientId = "";
    private static final String accessToken = "";

    public static void main(String[] args) {

        Set<UserFileChoices> ufc = new HashSet<UserFileChoices>();
        for (UserFileChoices choices : UserFileChoices.values()) {
            ufc.add(choices);
        }

        String decodedPath = null;
        try {
            String parentPath = new File(DonationTracker.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent();
            decodedPath = URLDecoder.decode(parentPath, "UTF-8");
            decodedPath = decodedPath + File.separator;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println("Files will be extracted to : " + decodedPath);
        User user = new User(clientId, accessToken, decodedPath, 10, 3, ufc);

        TrackEngine engine = new TrackEngine();
        engine.initEngine(user);
        engine.calculateTotalDonationsByDonator(user);

        while (true) {
            engine.requestRecentDonations(user);
            System.out.println(new Date().toString() + " listening...");
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
