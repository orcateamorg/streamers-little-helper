package org.orca.app.donationTracker.engine;

import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.lang3.StringUtils;
import org.orca.app.donationTracker.enums.SortTypeEnum;
import org.orca.app.donationTracker.enums.choices.UserFileChoices;
import org.orca.app.donationTracker.modals.Tip;
import org.orca.app.donationTracker.modals.TipWrapper;
import org.orca.app.donationTracker.modals.User;
import org.orca.app.donationTracker.modals.responses.TipResponse;
import org.orca.app.donationTracker.services.ApiConnector;
import org.orca.app.donationTracker.services.impl.ApiConnectorStreamTipImpl;
import org.orca.app.donationTracker.utils.FileUtil;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

public class TrackEngine {

    private final int maxListSize = 25;

    //file names
    private final String top = "top";
    private final String recent = "recent";
    private final String session = "session";
    private final String donator = "donator.xml";

    //lists to hold data orderly
    private final LinkedList<Tip> recentDonatList = new LinkedList<Tip>();
    private final LinkedList<Tip> topDonatList = new LinkedList<Tip>();

    private final LinkedList<Tip> sessionRecentDonatList = new LinkedList<Tip>();
    private final LinkedList<Tip> sessionTopDonatList = new LinkedList<Tip>();

    private final LinkedList<Tip> announceDonatList = new LinkedList<Tip>();
    private final Map<String, TipWrapper> donatorEmailTotalMap = new HashMap<String, TipWrapper>(0);

    //singleton of comparator, formatter etc.
    private final DecimalFormat df = new DecimalFormat("#,##0.00");
    private final ComparatorChain tipComparatorChain = new ComparatorChain();

    //reusable components
    private ApiConnector connector;
    private Date latestRequestDate = null;

    /**
     * Initializes the lists for engine then makes the requested files.
     *
     * @param user
     */
    public void initEngine(User user) {
        connector = new ApiConnectorStreamTipImpl();

        //initializes the comparator chain used for the top donation sorting
        initComparatorChain();

        //fill the recent list with 25 donations
        TipResponse res = connector.getTipResponse(user, SortTypeEnum.DATE);
        if (isResponseSuccessful(res.getStatus())) {
            recentDonatList.addAll(res.getTips());
            if (!recentDonatList.isEmpty()) {
                latestRequestDate = recentDonatList.getFirst().getDate();
            }
        }

        //fill the top donation list with 25 donations
        res = connector.getTipResponse(user, SortTypeEnum.AMOUNT);
        if (isResponseSuccessful(res.getStatus())) {
            topDonatList.addAll(res.getTips());
            Collections.sort(topDonatList, tipComparatorChain);
        }

        makeAllFiles(user);
    }

    public void calculateTotalDonationsByDonator(User user) {
        Integer offset = 0;
        TipResponse res = null;
        while (res == null || res.get_count() == maxListSize) {
            res = connector.getTipResponse(user, maxListSize, offset, SortTypeEnum.DATE);
            if (isResponseSuccessful(res.getStatus())) {
                offset = offset + maxListSize;
                for (Tip tip : res.getTips()) {
                    if (donatorEmailTotalMap.containsKey(tip.getEmail())) {
                        TipWrapper oldData = donatorEmailTotalMap.get(tip.getEmail());
                        oldData.setAmount(oldData.getAmount() + tip.getAmount());
                        donatorEmailTotalMap.put(tip.getEmail(), oldData);
                    } else {
                        TipWrapper newData = new TipWrapper(tip.getEmail(), tip.getFirstName(), tip.getIpAddress(), tip.getLastName(), tip.getAmount(), tip.getCurrencyCode());
                        donatorEmailTotalMap.put(tip.getEmail(), newData);
                    }
                }
            }
        }

        List<TipWrapper> tipList = new ArrayList<TipWrapper>(0);
        tipList.addAll(donatorEmailTotalMap.values());
        Collections.sort(tipList, new Comparator<TipWrapper>() {
            @Override
            public int compare(TipWrapper o1, TipWrapper o2) {
                return o2.getAmount().compareTo(o1.getAmount());
            }
        });

        StringBuffer buffer = new StringBuffer();
        for (TipWrapper tip : tipList) {
            buffer.append(tip.getFirstName() + " " + tip.getLastName() + " - " + tip.getEmail() + ": " + df.format(tip.getAmount()) + " " + tip.getCurrencyCode());
            buffer.append("; ");
        }

        try {
            FileUtil.writeDataToFile(user.getFolderName() + "allDonations.txt", buffer.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(new Date().toString() + " make allDonations.txt complete.");
    }

    /**
     * This method will be called by a period and updates the lists.
     *
     * @param user
     */
    public synchronized void requestRecentDonations(User user) {
        TipResponse res = connector.getTipResponse(user, latestRequestDate, SortTypeEnum.DATE);
        if (isResponseSuccessful(res.getStatus())) {
            List<Tip> resTipList = res.getTips();
            //if list is not null and not empty, insert the donations in required lists.
            if (resTipList != null && !resTipList.isEmpty()) {
                //we should revert the list order to process data orderly
                Collections.reverse(resTipList);

                synchronized (announceDonatList) {
                    announceDonatList.addAll(resTipList);
                }
                //insert the new tips to the required lists
                for (Tip tip : resTipList) {
                    //we add the recent donation to the recentDonatList and sessionRecentDonatList
                    //and remove the 26th donation from the list to keep it 25
                    recentDonatList.push(tip);
                    if (recentDonatList.size() > maxListSize) {
                        recentDonatList.removeLast();
                    }
                    sessionRecentDonatList.push(tip);
                    if (sessionRecentDonatList.size() > maxListSize) {
                        sessionRecentDonatList.removeLast();
                    }

                    //we add the donation to the sessionTopDonatList
                    //and remove the 26th donation from the list to keep it 25
                    sessionTopDonatList.add(tip);
                    Collections.sort(sessionTopDonatList, tipComparatorChain);
                    if (sessionTopDonatList.size() > maxListSize) {
                        sessionTopDonatList.removeLast();
                    }


                    //resetting the latest accessed time
                    latestRequestDate = tip.getDate();
                    //if the topDonatList is empty, insert the first donation
                    if (topDonatList.isEmpty() || topDonatList.size() < maxListSize) {
                        topDonatList.add(tip);
                        //reorder the list by amount
                        Collections.sort(topDonatList, tipComparatorChain);
                        //if the donation amount is high enough, we insert the donation to the topDonatList
                    } else if (topDonatList.getLast().getAmount() < tip.getAmount()) {
                        //we add the donation to the list
                        topDonatList.add(tip);
                        //reorder the list by amount
                        Collections.sort(topDonatList, tipComparatorChain);
                        //remove the 26th donation from the list to keep it 25
                        if (topDonatList.size() > maxListSize) {
                            topDonatList.removeLast();
                        }
                    }
                }
                //if all goes well, make the files again
                makeAllFiles(user);
            }
        }
    }

    /**
     * This method calls the files requested from the user
     */

    private synchronized void makeAllFiles(User user) {
        List<Tip> tempList = new ArrayList<Tip>(0);
        String path = user.getFolderName();
        if (user.getUserFileChoicesSet().contains(UserFileChoices.RECENT_DONATOR)) {
            tempList.clear();
            //if recentDonatList is not empty, fill the list
            if (!recentDonatList.isEmpty()) {
                tempList.add(recentDonatList.getFirst());
            }
            beautifyAndWriteFile(tempList, path + recent + donator, true);
            System.out.println(new Date().toString() + " make " + recent + donator + " complete.");
        }
        if (user.getUserFileChoicesSet().contains(UserFileChoices.RECENTX_DONATOR)) {
            tempList.clear();
            //if recentDonatList is not empty, fill the list
            if (!recentDonatList.isEmpty()) {
                Integer to = user.getRecentTipCount() > recentDonatList.size() ? recentDonatList.size() : user.getRecentTipCount();
                tempList.addAll(recentDonatList.subList(0, to));
            }
            beautifyAndWriteFile(tempList, path + recent + user.getRecentTipCount() + donator, false);
            System.out.println(new Date().toString() + " make " + recent + user.getRecentTipCount() + donator + " complete.");
        }
        if (user.getUserFileChoicesSet().contains(UserFileChoices.SESSION_RECENT_DONATOR)) {
            tempList.clear();
            //if sessionRecentDonatList is not empty, fill the list
            if (!sessionRecentDonatList.isEmpty()) {
                tempList.add(sessionRecentDonatList.getFirst());
            }
            beautifyAndWriteFile(tempList, path + session + recent + donator, true);
            System.out.println(new Date().toString() + " make " + session + recent + donator + " complete.");
        }
        if (user.getUserFileChoicesSet().contains(UserFileChoices.SESSION_RECENTX_DONATOR)) {
            tempList.clear();
            //if sessionRecentDonatList is not empty, fill the list
            if (!sessionRecentDonatList.isEmpty()) {
                Integer to = user.getRecentTipCount() > sessionRecentDonatList.size() ? sessionRecentDonatList.size() : user.getRecentTipCount();
                tempList.addAll(sessionRecentDonatList.subList(0, to));
            }
            beautifyAndWriteFile(tempList, path + session + recent + user.getRecentTipCount() + donator, false);
            System.out.println(new Date().toString() + " make " + session + recent + user.getRecentTipCount() + donator + " complete.");
        }
        if (user.getUserFileChoicesSet().contains(UserFileChoices.TOP_DONATOR)) {
            tempList.clear();
            //if topDonatList is not empty, fill the list
            if (!topDonatList.isEmpty()) {
                tempList.add(topDonatList.getFirst());
            }
            beautifyAndWriteFile(tempList, path + top + donator, true);
            System.out.println(new Date().toString() + " make " + top + donator + " complete.");
        }
        if (user.getUserFileChoicesSet().contains(UserFileChoices.TOPX_DONATOR)) {
            tempList.clear();
            //if topDonatList is not empty, fill the list
            if (!topDonatList.isEmpty()) {
                Integer to = user.getTopTipCount() > topDonatList.size() ? topDonatList.size() : user.getTopTipCount();
                tempList.addAll(topDonatList.subList(0, to));
            }
            beautifyAndWriteFile(tempList, path + top + user.getTopTipCount() + donator, false);
            System.out.println(new Date().toString() + " make " + top + user.getTopTipCount() + donator + " complete.");
        }
        if (user.getUserFileChoicesSet().contains(UserFileChoices.SESSION_TOP_DONATOR)) {
            tempList.clear();
            //if sessionTopDonatList is not empty, fill the list
            if (!sessionTopDonatList.isEmpty()) {
                tempList.add(sessionTopDonatList.getFirst());
            }
            beautifyAndWriteFile(tempList, path + session + top + donator, true);
            System.out.println(new Date().toString() + " make " + session + top + donator + " complete.");
        }
        if (user.getUserFileChoicesSet().contains(UserFileChoices.SESSION_TOPX_DONATOR)) {
            tempList.clear();
            //if sessionTopDonatList is not empty, fill the list
            if (!sessionTopDonatList.isEmpty()) {
                Integer to = user.getTopTipCount() > sessionTopDonatList.size() ? sessionTopDonatList.size() : user.getTopTipCount();
                tempList.addAll(sessionTopDonatList.subList(0, to));
            }
            beautifyAndWriteFile(tempList, path + session + top + user.getTopTipCount() + donator, false);
            System.out.println(new Date().toString() + " make " + session + top + user.getTopTipCount() + donator + " complete.");
        }
    }

    /**
     * This method formats the tip data as requested by the user and writes to the file.
     * Filename should contain the absolute address.
     * noLastSeperator is used to determine whether the last "; " should be put to the file or not.
     *
     * @param tipList
     * @param fileName
     * @param noLastSeperator
     */
    private void beautifyAndWriteFile(List<Tip> tipList, String fileName, boolean noLastSeperator) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<xsplit>");
        for (Tip tip : tipList) {
            String username = StringUtils.replace(tip.getUsername(), "&lt;", "<");
            buffer.append(username + ": " + df.format(tip.getAmount()) + " " + tip.getCurrencyCode());
            buffer.append("; ");
        }

        if (noLastSeperator && !tipList.isEmpty()) {
            int size = buffer.length();
            buffer.delete(size - 2, size);
        }

        buffer.append("</xsplit>");
        try {
            FileUtil.writeDataToFile(fileName, buffer.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializes the tipComparatorChain to sort the tips as amount first, if equals, date later.
     */
    private void initComparatorChain() {
        //we define the amount comparator
        Comparator<Tip> amountComparator = new Comparator<Tip>() {
            @Override
            public int compare(Tip o1, Tip o2) {
                return o2.getAmount().compareTo(o1.getAmount());
            }
        };

        //we define the date comparator
        Comparator<Tip> dateComparator = new Comparator<Tip>() {
            @Override
            public int compare(Tip o1, Tip o2) {
                return o2.getDate().compareTo(o1.getDate());
            }
        };

        //the first added comparator will be compared first. In case of equality, the second comparator is used.
        tipComparatorChain.addComparator(amountComparator);
        tipComparatorChain.addComparator(dateComparator);
    }

    /**
     * Returns true if statusCode is 200.
     *
     * @param statusCode
     * @return
     */
    private boolean isResponseSuccessful(Integer statusCode) {
        return statusCode.equals(200);
    }
}
