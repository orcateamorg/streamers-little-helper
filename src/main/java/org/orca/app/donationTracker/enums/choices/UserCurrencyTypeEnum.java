package org.orca.app.donationTracker.enums.choices;

public enum UserCurrencyTypeEnum {

    CURRENCY_CODE,
    CURRENCY_SYMBOL;

}
