package org.orca.app.donationTracker.enums.choices;

public enum UserFileChoices {

    TOP_DONATOR("Top Donator"),
    TOPX_DONATOR("Top X Donator"),

    SESSION_TOP_DONATOR("Session Top Donator"),
    SESSION_TOPX_DONATOR("Session Top X Donator"),

    RECENT_DONATOR("Last Donator"),
    RECENTX_DONATOR("Recent X Donator"),

    SESSION_RECENT_DONATOR("Session Last Donator"),
    SESSION_RECENTX_DONATOR("Session Recent X Donator");

    private String val;

    UserFileChoices(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
