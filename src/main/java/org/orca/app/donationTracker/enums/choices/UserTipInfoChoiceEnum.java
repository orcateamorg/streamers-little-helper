package org.orca.app.donationTracker.enums.choices;

public enum UserTipInfoChoiceEnum {

    USERNAME("Username"),
    AMOUNT("Amount"),
    CURRENCY("Currency"),
    NOTE("Note"),
    DATE("Date"),
    MIN_TIP_THRESHOLD("Minimum Tip Threshold");

    private String val;

    UserTipInfoChoiceEnum(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
