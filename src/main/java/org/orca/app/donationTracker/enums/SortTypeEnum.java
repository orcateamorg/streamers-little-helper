package org.orca.app.donationTracker.enums;

public enum SortTypeEnum {

    DATE("date"),

    AMOUNT("amount");

    private String val;

    SortTypeEnum(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
