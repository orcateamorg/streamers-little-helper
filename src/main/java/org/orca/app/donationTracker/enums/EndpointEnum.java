package org.orca.app.donationTracker.enums;

public enum EndpointEnum {

    TIPS("/tips");

    private String val;

    EndpointEnum(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
