package org.orca.app.donationTracker.enums.choices;

public enum UserDateTypeEnum {

    DDMMYYYY("day/month/year"),
    MMDDYYYY("month/day/year"),
    DDMMYYYYHHMM("day/month/year minute:second"),
    MMDDYYYYHHMM("month/day/year minute:second");
    //todo these choices can be increased

    private String val;

    UserDateTypeEnum(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
