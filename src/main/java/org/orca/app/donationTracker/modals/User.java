package org.orca.app.donationTracker.modals;

import org.orca.app.donationTracker.enums.choices.UserCurrencyTypeEnum;
import org.orca.app.donationTracker.enums.choices.UserDateTypeEnum;
import org.orca.app.donationTracker.enums.choices.UserFileChoices;
import org.orca.app.donationTracker.enums.choices.UserTipInfoChoiceEnum;

import java.util.HashSet;
import java.util.Set;

public class User {

    private String clientId;
    private String accessToken;

    private String folderName;

    private Integer recentTipCount;
    private Integer topTipCount;

    private UserCurrencyTypeEnum currencyType;
    private UserDateTypeEnum dateType;

    private Set<UserTipInfoChoiceEnum> userTipInfoChoicesSet = new HashSet<UserTipInfoChoiceEnum>(0);
    private Set<UserFileChoices> userFileChoicesSet = new HashSet<UserFileChoices>(0);

    public User() {
    }

    public User(String clientId, String accessToken, String folderName, Integer recentTipCount, Integer topTipCount, Set<UserFileChoices> userFileChoicesSet) {
        this.clientId = clientId;
        this.accessToken = accessToken;
        this.folderName = folderName;
        this.recentTipCount = recentTipCount;
        this.topTipCount = topTipCount;
        this.userFileChoicesSet = userFileChoicesSet;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public Integer getRecentTipCount() {
        return recentTipCount;
    }

    public void setRecentTipCount(Integer recentTipCount) {
        this.recentTipCount = recentTipCount;
    }

    public Integer getTopTipCount() {
        return topTipCount;
    }

    public void setTopTipCount(Integer topTipCount) {
        this.topTipCount = topTipCount;
    }

    public UserCurrencyTypeEnum getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(UserCurrencyTypeEnum currencyType) {
        this.currencyType = currencyType;
    }

    public UserDateTypeEnum getDateType() {
        return dateType;
    }

    public void setDateType(UserDateTypeEnum dateType) {
        this.dateType = dateType;
    }

    public Set<UserTipInfoChoiceEnum> getUserTipInfoChoicesSet() {
        return userTipInfoChoicesSet;
    }

    public void setUserTipInfoChoicesSet(Set<UserTipInfoChoiceEnum> userTipInfoChoicesSet) {
        this.userTipInfoChoicesSet = userTipInfoChoicesSet;
    }

    public Set<UserFileChoices> getUserFileChoicesSet() {
        return userFileChoicesSet;
    }

    public void setUserFileChoicesSet(Set<UserFileChoices> userFileChoicesSet) {
        this.userFileChoicesSet = userFileChoicesSet;
    }

    @Override
    public String toString() {
        return "client_id=" + clientId + "&access_token=" + accessToken;
    }
}
