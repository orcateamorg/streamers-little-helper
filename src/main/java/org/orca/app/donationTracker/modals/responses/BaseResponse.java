package org.orca.app.donationTracker.modals.responses;

public class BaseResponse {

    private int _count;
    private int _limit;
    private int _offset;
    private int status;
    private String message;

    public int get_count() {
        return _count;
    }

    public void set_count(int _count) {
        this._count = _count;
    }

    public int get_limit() {
        return _limit;
    }

    public void set_limit(int _limit) {
        this._limit = _limit;
    }

    public int get_offset() {
        return _offset;
    }

    public void set_offset(int _offset) {
        this._offset = _offset;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
