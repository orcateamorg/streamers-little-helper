package org.orca.app.donationTracker.modals.responses;

import org.orca.app.donationTracker.modals.Tip;

import java.util.ArrayList;
import java.util.List;

public class TipResponse extends BaseResponse {

    private List<Tip> tips = new ArrayList<Tip>(0);

    public List<Tip> getTips() {
        return tips;
    }

    public void setTips(List<Tip> tips) {
        this.tips = tips;
    }
}
