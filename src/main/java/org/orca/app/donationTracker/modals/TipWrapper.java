package org.orca.app.donationTracker.modals;

public class TipWrapper {

    private String email;
    private String firstName;
    private String ipAddress;
    private String lastName;
    private Double amount;
    private String currencyCode;

    public TipWrapper(String email, String firstName, String ipAddress, String lastName, Double amount, String currencyCode) {
        this.email = email;
        this.firstName = firstName;
        this.ipAddress = ipAddress;
        this.lastName = lastName;
        this.amount = amount;
        this.currencyCode = currencyCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName + " - " + email + ": " + amount + " - " + currencyCode;
    }

}
