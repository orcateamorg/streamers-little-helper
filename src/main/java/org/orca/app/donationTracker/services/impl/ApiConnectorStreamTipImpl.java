package org.orca.app.donationTracker.services.impl;

import com.google.gson.GsonBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.orca.app.donationTracker.enums.EndpointEnum;
import org.orca.app.donationTracker.enums.SortTypeEnum;
import org.orca.app.donationTracker.modals.User;
import org.orca.app.donationTracker.modals.responses.TipResponse;
import org.orca.app.donationTracker.services.ApiConnector;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ApiConnectorStreamTipImpl implements ApiConnector {

    private static final String schema = "https";
    private static final String host = "streamtip.com";
    private static final String path = "/api";

    /**
     * Generates the connection string with the given parameters.
     *
     * @param user
     * @param sortType
     * @return
     */
    @Override
    public TipResponse getTipResponse(User user, SortTypeEnum sortType) {
        return getTipResponse(user, null, null, null, sortType);
    }

    /**
     * Generates the connection string with the given parameters.
     *
     * @param user
     * @param dateFrom
     * @param sortType
     * @return
     */
    @Override
    public TipResponse getTipResponse(User user, Date dateFrom, SortTypeEnum sortType) {
        return getTipResponse(user, dateFrom, null, null, sortType);
    }

    /**
     * Generates the connection string with the given parameters.
     *
     * @param user
     * @param limit
     * @param offset
     * @param sortType
     * @return
     */
    @Override
    public TipResponse getTipResponse(User user, Integer limit, Integer offset, SortTypeEnum sortType) {
        return getTipResponse(user, null, limit, offset, sortType);
    }

    /**
     * Generates the connection string with the given parameters.
     *
     * @param user
     * @param dateFrom
     * @param limit
     * @param offset
     * @param sortType
     * @return
     */
    @Override
    public TipResponse getTipResponse(User user, Date dateFrom, Integer limit, Integer offset, SortTypeEnum sortType) {
        TipResponse tipResponse = new TipResponse();

        try {
            HttpGet httpGet = new HttpGet(getConnectionURI(user, dateFrom, limit, offset, sortType));
            httpGet.setHeader("charset", "UTF-8");

            HttpClient httpclient = HttpClientBuilder.create().build();

            HttpResponse response = httpclient.execute(httpGet);
            response.setLocale(new Locale("tr", "TR"));
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream is = entity.getContent();
                InputStreamReader reader = new InputStreamReader(is);

                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                tipResponse = gsonBuilder.create().fromJson(reader, TipResponse.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("resuming...");
            return tipResponse;
        }

        return tipResponse;
    }

    /**
     * Generates the connection uri with the parameters provided.
     *
     * @param user
     * @param dateFrom
     * @param limit
     * @param offset
     * @param sortType
     * @return
     * @throws URISyntaxException
     */
    private URI getConnectionURI(User user, Date dateFrom, Integer limit, Integer offset, SortTypeEnum sortType) throws URISyntaxException {
        URIBuilder builder = new URIBuilder()
                .setScheme(schema)
                .setHost(host)
                .setPath(path + EndpointEnum.TIPS.getVal());

        builder.addParameter("client_id", user.getClientId());
        builder.addParameter("access_token", user.getAccessToken());

        if (dateFrom != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            builder.setParameter("date_from", sdf.format(dateFrom));
        }
        if (limit != null && limit > 0) {
            builder.setParameter("limit", limit.toString());
        }
        if (offset != null && offset > 0) {
            builder.setParameter("offset", offset.toString());
        }
        if (sortType != null) {
            builder.setParameter("sort_by", sortType.getVal());
        }

        return builder.build();
    }
}
