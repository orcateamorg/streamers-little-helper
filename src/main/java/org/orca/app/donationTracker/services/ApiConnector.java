package org.orca.app.donationTracker.services;

import org.orca.app.donationTracker.enums.SortTypeEnum;
import org.orca.app.donationTracker.modals.User;
import org.orca.app.donationTracker.modals.responses.TipResponse;

import java.util.Date;

public interface ApiConnector {

    public TipResponse getTipResponse(User user, SortTypeEnum sortType);

    public TipResponse getTipResponse(User user, Date dateFrom, SortTypeEnum sortType);

    public TipResponse getTipResponse(User user, Integer limit, Integer offset, SortTypeEnum sortType);

    public TipResponse getTipResponse(User user, Date dateFrom, Integer limit, Integer offset, SortTypeEnum sortType);

}
